# -*- coding: utf-8 -*-
import pandas as pd
import json
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
import requests
from bs4 import BeautifulSoup as bs
from log import get_logger
from selenium import webdriver
import os
import time
import ast
from info import infoContact
from selenium.webdriver.common.keys import Keys
from sqlalchemy import create_engine
import re

MAIN_URL = "https://dothi.net/"
USER = ""
PASSWORD = ""
lst_title = []
lst_link = []
lst_name = []
lst_phone = []
lst_email = []
lst_address = []
lst_last_update = []
lst_code = []
lst_post_date = []
lst_expiration_date = []
lst_price = []
lst_area = []
lst_sector = []
lst_category = []
lst_unit_area = []

sector = 'Hồ Chí Minh'
area = '150-200 m2'
price = '> 30 tỷ'

dict_area = {
    0: "Không xác định",
    1: "<= 30 m2",
    2: "30-50 m2",
    3: "50-80 m2",
    4: "80-100 m2",
    5: "100-150 m2",
    6: "150-200 m2",
    7: "200-250 m2",
    8: "250-300 m2",
    9: "300-500 m2",
    10: ">=500 m2"
}

dict_price = {
    0: "Thỏa thuận",
    1: "< 500 triệu",
    2: "500 - 800 triệu",
    3: "800 - 1 tỷ",
    4: "1 - 2 tỷ",
    5: "2 - 3 tỷ",
    6: "3 - 5 tỷ",
    7: "5 - 7 tỷ",
    8: "7 - 10 tỷ",
    9: "10 - 20 tỷ",
    10: "20 - 30 tỷ",
    11: "> 30 tỷ"
}


def crawler(target_element, driver):
    if not target_element:
        return

    for e in target_element.findChildren('li'):
        link = MAIN_URL + e.find('a', href=True)['href']
        setup_data(link, driver)


def crowl_data_search(href, *args):
    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--incognito')
    options.add_argument("--start-maximized")
    dir_path = os.path.dirname(os.path.realpath(__file__))
    driver = webdriver.Chrome(os.path.join(dir_path, "chromedriver"), options=options)
    driver.get(href)
    if args[0]:
        filterSector = "//select[@id='ddlCity']/option[text()=" + "'" + args[0] + "'" + "]"""
        driver.find_element_by_xpath(filterSector).click()
    if args[1]:
        for key in dict_area:
            if dict_area.get(key) == args[1]:
                filterCategory = "//*[@id=" + "\"cboArea\"" + "]/div[1]/span[1]"
                driver.find_element_by_xpath(filterCategory).click()
                filter_li = "//*[@id=" + "\"cboArea\"" + "]/div[2]/ul/li[" + str(key + 2) + "]"
                driver.find_element_by_xpath(filter_li).click()

    if args[2]:
        for key in dict_price:
            if dict_price.get(key) == args[2]:
                filterCategory = "//*[@id=" + "\"cboPrice\"" + "]/div[1]/span[1]"
                driver.find_element_by_xpath(filterCategory).click()
                filter_li = "//*[@id=" + "\"cboPrice\"" + "]/div[2]/ul/li[" + str(key + 2) + "]"
                driver.find_element_by_xpath(filter_li).click()

    driver.find_element_by_class_name('btnsearch').click()
    fistUrl = driver.current_url
    html = get_html(fistUrl)
    soup = bs(html, "html.parser")
    target_element = soup.find('div', {'class': 'for-user listProduct'})
    if target_element:
        i = 1
        while target_element.find('li'):
            lst_title.clear()
            lst_link.clear()
            lst_name.clear()
            lst_phone.clear()
            lst_email.clear()
            lst_address.clear()
            lst_last_update.clear()
            lst_code.clear()
            lst_post_date.clear()
            lst_expiration_date.clear()
            lst_price.clear()
            lst_area.clear()
            lst_sector.clear()
            lst_category.clear()
            lst_unit_area.clear()

            lastUrl = "/" + 'p' + str(i) + ".htm"
            url = str(fistUrl).replace('.htm', lastUrl)
            html = get_html(url)
            soup = bs(html, "html.parser")
            target_element = soup.find('div', {'class': 'for-user listProduct'})
            crawler(target_element, driver)
            insert_db()
            print('insert success page' + str(i))
            i = i + 1

        print('=======================DONE===========================')


def insert_db():
    df1 = pd.DataFrame({"code": lst_code,
                        "title": lst_title,
                        "link": lst_link,
                        "sector": lst_sector,
                        "category": lst_category,
                        "name": lst_name,
                        "phone": lst_phone,
                        "email": lst_email,
                        "address": lst_address,
                        "post_date": lst_post_date,
                        "expiration_date": lst_expiration_date,
                        "price": lst_price,
                        "area": lst_area,
                        "unit_area": lst_unit_area})

    engine = create_engine("mysql+pymysql://root:123456@localhost/python_test")
    df1.to_sql('db_info', con=engine, if_exists='append', index=False)


def convert_date(str_date):
    # str_date format is 08/11/1998
    arr = str_date.split("/")
    day = arr[0].strip()
    month = arr[1].strip()
    year = arr[2].strip()
    return year + "-" + month + "-" + day


def convert_price_page_vndiaoc(input_price):
    if "thỏa thuận" in input_price.lower():
        return 0
    else:
        price = re.findall(r"[-+]?\d*\.\d+|\d+", input_price)
        str_zero = "{0}"

        length_of_number = 0
        app_zero = 0

        if "." in input_price:
            length_of_number = len(str(price).split(".")[1].replace("']", ""))

        if "tỷ" in input_price.lower():
            app_zero = 9 - int(length_of_number)
        elif "triệu" in input_price.lower():
            app_zero = 6 - int(length_of_number)
        elif "nghìn" in input_price.lower():
            app_zero = 3 - int(length_of_number)

        for i in range(0, app_zero):
            str_zero += "0"
        return str_zero.format(str(price[0]).replace(".", ""))


def setup_data(href, driver):
    name = ''
    address = ''
    phone_number = ''
    email = ''
    code = ''
    type = ''
    post_date = ''
    expiration_date = ''

    child_html = get_html(href)
    if not child_html:
        return
    soup = bs(child_html, "html.parser")

    title = soup.find('h1').text.strip()
    title = check_regex(title)
    dt_address = soup.find('div', {'class': 'pd-location'}).text.strip()

    if len(dt_address.split(':')) > 1:
        address = dt_address.split(':')[1]
    data = soup.find('div', {'class': 'pd-price'})

    price = convert_price_page_vndiaoc(str(data.text.strip().split('\r\n')[1]))

    areas = data.text.strip().split('\r\n')[3]

    lst_dt = areas.split()
    area = lst_dt[0]
    unit_area = lst_dt[1]

    driver.get(href)
    table1 = driver.find_element_by_xpath("//*[@id=\"tbl1\"]").text
    table2 = driver.find_element_by_xpath("//*[@id=\"tbl2\"]").text

    data_table1 = table1.split('\n')
    data_table2 = table2.split('\n')

    for s in data_table2:
        if 'Tên liên lạc' in s:
            name = s.replace('Tên liên lạc', '')
        elif 'Địa chỉ' in s:
            if not address:
                address = s.replace('Địa chỉ', '')
        elif 'Điện thoại' in s:
            phone_number = s.replace('Điện thoại', '')
        elif 'Di động' in s:
            phone_number = s.replace('Di động', '')
        elif 'Email' in s:
            email = s.replace('Email', '')

    for s2 in data_table1:
        if 'Mã số' in s2:
            code = s2.replace('Mã số', '')
        elif 'Loại tin rao' in s2:
            type = s2.replace('Loại tin rao', '')
        elif 'Ngày đăng tin' in s2:
            post_date = convert_date(str(s2.replace('Ngày đăng tin', '')))
        elif 'Ngày hết hạn' in s2:
            expiration_date = convert_date(str(s2.replace('Ngày hết hạn', '')))
        else:
            continue

    lst_title.append(title)
    lst_sector.append(sector)
    lst_code.append(code)
    lst_name.append(name)
    lst_link.append(href)
    lst_area.append(area)
    lst_address.append(address)
    lst_email.append(email)
    lst_expiration_date.append(expiration_date)
    lst_post_date.append(post_date)
    lst_phone.append(phone_number)
    lst_category.append(type)
    lst_price.append(price)
    lst_unit_area.append(unit_area)


def get_html(url):
    req = requests.get(url)
    if not req.ok:
        return

    return req.text


# Press the green button in the gutter to run the script.
def check_regex(data):
    title = ''
    find = re.findall(
        r",|\b\S*[a-z 0-9 AĂÂÁẮẤÀẰẦẢẲẨÃẴẪẠẶẬĐEÊÉẾÈỀẺỂẼỄẸỆIÍÌỈĨỊOÔƠÓỐỚÒỒỜỎỔỞÕỖỠỌỘỢUƯÚỨÙỪỦỬŨỮỤỰYÝỲỶỸỴAĂÂÁẮẤÀẰẦẢẲẨÃẴẪẠẶẬĐE"
        r"ÊÉẾÈỀẺỂẼỄẸỆIÍÌỈĨỊOÔƠÓỐỚÒỒỜỎỔỞÕỖỠỌỘỢUƯÚỨÙỪỦỬŨỮỤỰYÝỲỶỸỴAĂÂÁẮẤÀẰẦẢẲẨÃẴẪẠẶẬĐEÊÉẾÈỀẺỂẼỄẸỆIÍÌỈĨỊOÔƠÓỐỚÒỒỜỎỔỞÕỖỠỌỘỢU"
        r"ƯÚỨÙỪỦỬŨỮỤỰYÝỲỶỸỴAĂÂÁẮẤÀẰẦẢẲẨÃẴẪẠẶẬĐEÊÉẾÈỀẺỂẼỄẸỆIÍÌỈĨỊOÔƠÓỐỚÒỒỜỎỔỞÕỖỠỌỘỢUƯÚỨÙỪỦỬŨỮỤỰYÝỲỶỸỴAĂÂÁẮẤÀẰẦẢẲẨÃẴẪẠẶẬĐ"
        r"EÊÉẾÈỀẺỂẼỄẸỆIÍÌỈĨỊOÔƠÓỐỚÒỒỜỎỔỞÕỖỠỌỘỢUƯÚỨÙỪỦỬŨỮỤỰYÝỲỶỸỴAĂÂÁẮẤÀẰẦẢẲẨÃẴẪẠẶẬĐEÊÉẾÈỀẺỂẼỄẸỆIÍÌỈĨỊOÔƠÓỐỚÒỒỜỎỔỞÕỖỠỌỘỢ"
        r"UƯÚỨÙỪỦỬŨỮỤỰYÝỲỶỸỴA-Z]+\S*",
        data.upper())

    for s in find:
        title += s
    return title


if __name__ == '__main__':
    crowl_data_search(MAIN_URL, sector, area, price)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
